import { spawn } from "child_process";
import asyncHandler from "express-async-handler";
import path from "path";
import axios from "axios";

const __dirname = path.resolve();

const jsonData = asyncHandler(async (req, res) => {
  const pythonFilePath = path.join(__dirname, "backend/data/content.py");
  const pythonFile = spawn("python", [pythonFilePath]);

  let outputData = ""; // Variable to store the output data

  // Listen for the Python file's output
  pythonFile.stdout.on("data", (data) => {
    outputData += data.toString(); // Append the data to the outputData variable
  });

  // Listen for any errors that occur during execution
  pythonFile.stderr.on("data", (data) => {
    console.error(data.toString());
  });

  // Listen for the Python file's completion
  pythonFile.on("close", (code) => {
    if (code === 0) {
      try {
        const parsedOutput = JSON.parse(outputData);
        const parsedData = parsedOutput.variable;
        parsedData.forEach(async (obj) => {
          obj["name"] = "Bitstarz";
        });
        console.log(parsedData);

        res.send(parsedData);
      } catch (error) {
        console.error("Error parsing JSON output:", error);
        res.status(500).send("Error occurred");
      }
    } else {
      console.error(`Python file exited with code ${code}`);
      res.status(500).send("Error occurred");
    }
  });
});

export { jsonData };
