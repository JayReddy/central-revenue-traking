import { Flex, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Flex
      bg="linear-gradient(to right, #0f0c29, #302b63, #24243e)"
      as="footer"
      justifyContent="center"
      py="5"
    >
      <Text color="white">
        Copyright {new Date().getFullYear()}. Central Revenue Tracking. All
        Rights Reserved.
      </Text>
    </Flex>
  );
};

export default Footer;
