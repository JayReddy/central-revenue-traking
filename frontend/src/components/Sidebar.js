import {
  Flex,
  Icon,
  Link,
  List,
  Menu,
  MenuButton,
  MenuList,
  MenuGroup,
  MenuDivider,
} from "@chakra-ui/react";
import {
  AiOutlineDashboard,
  AiFillProfile,
  AiOutlineLogout,
} from "react-icons/ai";
import { FaBitcoin } from "react-icons/fa";
import { MdCasino } from "react-icons/md";
import { BsDatabaseAdd } from "react-icons/bs";
import { CgUserList } from "react-icons/cg";

import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { logout } from "../actions/userActions";

import { Link as RouterLink, useNavigate } from "react-router-dom";

const Sidebar = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const logoutHandler = () => {
    dispatch(logout());
    navigate("/");
  };

  return (
    <Flex
      flexDirection="column"
      justifyContent="flex-start"
      alignContent="center"
      gap="5%"
      wrap="wrap"
      py="12"
      bg="linear-gradient(to right, #0f0c29, #302b63, #24243e)"
      w="20%"
      h="100%"
      pos="fixed"
      as="h2"
      color="white"
      fontWeight="bold"
      fontSize="md"
      letterSpacing="wide"
    >
      <List spacing={10}>
        <Link
          as={RouterLink}
          to="/revenue"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          <Icon as={AiOutlineDashboard} mr="1" w="4" h="4" />
          Dashboard
        </Link>

        <Link
          as={RouterLink}
          to="/profile"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          <Icon as={AiFillProfile} mr="1" w="4" h="4" />
          Profile
        </Link>

        <Link
          as={RouterLink}
          to="/admin/userlist"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          <Icon as={CgUserList} mr="1" w="4" h="4" />
          Userlist
        </Link>

        <Link
          as={RouterLink}
          to="/revenue"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          <Icon as={BsDatabaseAdd} mr="1" w="4" h="4" />
          Overall Data
        </Link>

        <Link
          as={RouterLink}
          to="/revenue"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          {" "}
          <Menu>
            <MenuButton
              as={RouterLink}
              colorScheme="gray.800"
              textTransform="uppercase"
              _hover={{ color: "cyan" }}
            >
              <Icon as={FaBitcoin} mr="2" w="4" h="4" />
              Bitcoin Casino
            </MenuButton>
            <MenuList bgColor="gray.800" color="whiteAlpha.800">
              <Link
                as={RouterLink}
                to="/revenue"
                textTransform="uppercase"
                mr="5"
                display="flex"
                alignItems="center"
                wrap="wrap"
                _hover={{ color: "cyan" }}
                mt={{ base: 4, md: 0 }}
              >
                <MenuGroup title="Cryptowild Casino"></MenuGroup>
              </Link>
              <MenuDivider />

              <Link
                as={RouterLink}
                to="/revenue"
                textTransform="uppercase"
                mr="5"
                display="flex"
                alignItems="center"
                wrap="wrap"
                _hover={{ color: "cyan" }}
                mt={{ base: 4, md: 0 }}
              >
                <MenuGroup title="Bitstarz Casino"></MenuGroup>
              </Link>
            </MenuList>
          </Menu>
        </Link>

        <Link
          as={RouterLink}
          to="/admin/userlist"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          <Menu>
            <MenuButton
              as={RouterLink}
              colorScheme="gray.800"
              ml="-3.5"
              textTransform="uppercase"
              _hover={{ color: "cyan" }}
            >
              <Icon as={MdCasino} mr="2" w="4" h="4" />
              Real Money Casino
            </MenuButton>
            <MenuList bgColor="gray.800" color="whiteAlpha.800">
              <Link
                as={RouterLink}
                to="/revenue"
                textTransform="uppercase"
                mr="5"
                display="flex"
                alignItems="center"
                wrap="wrap"
                _hover={{ color: "cyan" }}
                mt={{ base: 4, md: 0 }}
              >
                <MenuGroup title="Lincoln Casino"></MenuGroup>
              </Link>
              <MenuDivider />

              <Link
                as={RouterLink}
                to="/revenue"
                textTransform="uppercase"
                mr="5"
                display="flex"
                alignItems="center"
                wrap="wrap"
                _hover={{ color: "cyan" }}
                mt={{ base: 4, md: 0 }}
              >
                <MenuGroup title="Intertops Casino"></MenuGroup>
              </Link>
              <MenuDivider />

              <Link
                as={RouterLink}
                to="/revenue"
                textTransform="uppercase"
                mr="5"
                display="flex"
                alignItems="center"
                wrap="wrap"
                _hover={{ color: "cyan" }}
                mt={{ base: 4, md: 0 }}
              >
                <MenuGroup title="CafeCasino and Slots.lv Casino"></MenuGroup>
              </Link>
            </MenuList>
          </Menu>
        </Link>

        <Link
          as={RouterLink}
          to="/"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
          onClick={logoutHandler}
        >
          <Icon as={AiOutlineLogout} mr="1" w="4" h="4" />
          Logout
        </Link>
      </List>
    </Flex>
  );
};

export default Sidebar;
