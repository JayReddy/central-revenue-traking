import { useEffect, useState } from "react";
import {
  Table,
  Tbody,
  Tr,
  Th,
  Td,
  Flex,
  Select,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
  Thead,
} from "@chakra-ui/react";
import axios from "axios";
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

const Revenue = () => {
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [contacts, setContacts] = useState(data);
  const [search, setSearch] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [showData, setShowData] = useState(false);
  const [usdRate, setUsdRate] = useState(0); // New state to hold the USD rate

  let [searchParam] = useSearchParams();
  let redirect = searchParam.get("redirect") || "/";

  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;

  useEffect(() => {
    if (!userInfo) {
      navigate(redirect);
    }
    revenueData();
    fetchUsdRate(); // Fetch the USD rate on component mount
  }, [userInfo, navigate, redirect]);

  const revenueData = async () => {
    try {
      const { data } = await axios.get("/api/jsonData");
      setData(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchUsdRate = async () => {
    try {
      const { data } = await axios.get("https://bitpay.com/api/rates/BTC/USD");
      const currentUsd = data.rate;
      setUsdRate(currentUsd); // Update the USD rate state with the fetched value
    } catch (error) {
      console.log(error);
    }
  };

  const handleViewClick = async () => {
    if (startDate && endDate) {
      try {
        await fetchUsdRate(); // Fetch the USD rate again
        setShowData(true);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const convertToUSD = (btcValue) => {
    return btcValue * usdRate; // Calculate the USD value using the fetched USD rate
  };

  const calculate40Percent = (usdValue) => {
    return usdValue * 0.4;
  };

  return (
    <>
      <Flex ml="10px" mr="10px">
        <Select
          placeholder="Select Casino"
          w="500px"
          border="solid black"
          mt="32px"
          id="dataTypeInput"
        >
          <option value="option1">Overall (For All Casinos)</option>
          <option value="option2">Bitstarz</option>
          <option value="option3">Crytowild</option>
          <option value="option4">Cointiply</option>
          <option value="option5">Lincoln</option>
          <option value="option6">Intertops</option>
          <option value="option7">CafeCasino and Slot.lv</option>
        </Select>

        <FormControl w="500px" ml="10px" mr="10px" id="fromDateInput">
          <FormLabel>Select Date:</FormLabel>
          <Input
            type="date"
            border="solid black"
            required
            value={startDate}
            onChange={(e) => setStartDate(e.target.value)}
            placeholder="Start Date"
          />
          <FormHelperText>Please select the Date</FormHelperText>
        </FormControl>
        <FormControl w="500px" ml="10px" mr="10px" id="toDateInput">
          <FormLabel>To:</FormLabel>
          <Input
            type="date"
            border="solid black"
            required
            value={endDate}
            onChange={(e) => setEndDate(e.target.value)}
            placeholder="End Date"
            min={startDate}
          />
          <FormHelperText>Please select the Date </FormHelperText>
        </FormControl>
        <Button
          colorScheme="blue"
          w="100px"
          ml="10px"
          mt="30px"
          id="viewButton"
          onClick={handleViewClick}
        >
          View
        </Button>
      </Flex>
      {showData && (
        <Table variant="striped">
          <Thead>
            <Tr>
              <Th>Date</Th>
              <Th>Brand ID</Th>
              <Th>Visits count</Th>
              <Th>Registrations count</Th>
              <Th>BTC First deposits count</Th>
              <Th>BTC Deposits sum</Th>
              <Th>BTC NGR</Th>
              <Th>In USD</Th>
              <Th>Earnings </Th>
            </Tr>
          </Thead>
          <Tbody>
            {data
              .filter((data) => {
                if (startDate && endDate) {
                  return data.Date >= startDate && data.Date <= endDate + 1;
                } else {
                  return true;
                }
              })
              .filter((data) => {
                return search === "" ? data : data.Date.includes(search);
              })
              .map((data, index) => {
                const btcValue = data["BTC NGR"];
                const usdValue = convertToUSD(btcValue);
                const percent40 = calculate40Percent(usdValue);

                return (
                  <Tr key={index}>
                    <Td>{data.Date}</Td>
                    <Td>{data["Brand ID"]}</Td>
                    <Td>{data["Visits count"]}</Td>
                    <Td>{data["Registrations count"]}</Td>
                    <Td>{data["BTC First deposits count"]}</Td>
                    <Td>{data["BTC Deposits sum"]}</Td>
                    <Td>{data["BTC NGR"]}</Td>
                    <Td>${usdValue.toFixed(2)}</Td>
                    <Td>${percent40.toFixed(2)}</Td>
                  </Tr>
                );
              })}
          </Tbody>
        </Table>
      )}
    </>
  );
};

export default Revenue;
